package com.example.todoist.presentation.scene.tasks

import android.view.View
import com.example.todoist.R
import com.example.todoist.data.model.Task
import com.example.todoist.databinding.SimpleTextItemBinding
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.viewbinding.BindableItem

class TasksAdapter : GroupAdapter<GroupieViewHolder>() {
  fun setItems(taskList: List<Task>) {
    taskList.forEach { add(TaskItem(it)) }
  }

  internal class TaskItem(private val task: Task) : BindableItem<SimpleTextItemBinding>() {
    override fun bind(viewBinding: SimpleTextItemBinding, position: Int) {
      viewBinding.simpleTextItem.text = task.content
    }

    override fun getLayout(): Int = R.layout.simple_text_item

    override fun initializeViewBinding(view: View): SimpleTextItemBinding =
      SimpleTextItemBinding.bind(view)
  }
}